class Pin < ActiveRecord::Base
     belongs_to :user 
     
      has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png" 
      
           validates :image,
    attachment_content_type: { content_type: /\Aimage\/.*\Z/ },
    attachment_size: { less_than: 5.megabytes }

     # validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/  
end
